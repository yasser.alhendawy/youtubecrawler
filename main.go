package main

import (
	"regexp"

	"gitlab.com/yasseralhendawy/youtubeCrawler/model"
	youtubecrawler "gitlab.com/yasseralhendawy/youtubeCrawler/youtubeCrawler"
)

//Validate to validate url links to be either channel or playlist
func Validate(url string) bool {
	ex := regexp.MustCompile("((http|https):\\/\\/|)(www\\.|)youtube\\.com\\/(channel\\/|user\\/)[a-zA-Z0-9\\-]{1,}\\/videos")

	if ex.MatchString(url) {
		model.MyLink.URL = url
		model.MyLink.LinkType = "channel"
		return true
	}
	ex = regexp.MustCompile("((http|https):\\/\\/|)(www\\.|)youtube\\.com\\/watch\\?v=[a-zA-Z0-9\\-]{1,}\\&list=[a-zA-Z0-9\\-_]{1,}")
	if ex.MatchString(url) {
		model.MyLink.URL = url
		model.MyLink.LinkType = "playlist"
		return true
	}
	return false
}
func main() {
	url := "https://www.youtube.com/watch?v=ztiHRiFXtoc&list=PLvFsG9gYFxY_2tiOKgs7b2lSjMwR89ECb"
	if Validate(url) {
		youtubecrawler.Crawler(url, false)
	}
}
