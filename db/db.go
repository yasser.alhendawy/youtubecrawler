package db

import (
	"context"

	"gitlab.com/yasseralhendawy/youtubeCrawler/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type configModel struct {
	mongouri  string
	Mongoname string
}

//Configer data
var configer = configModel{
	mongouri:  "mongodb://localhost:27017",
	Mongoname: "youtube",
}

//Client connection
var client, err = mongo.Connect(context.Background(), options.Client().ApplyURI(configer.mongouri))

//UpsertData to insert or update data
func UpsertData() error {
	q := bson.M{"url": model.MyLink.URL}
	q2 := bson.M{"$set": model.MyLink}
	data := client.Database(configer.Mongoname).Collection("youtubedata")
	_, err := data.UpdateOne(context.Background(), q, q2, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	return nil
}

//InsertData to insert or update data
func InsertData() error {
	data := client.Database(configer.Mongoname).Collection("youtubedata")
	_, err := data.InsertOne(context.Background(), model.MyLink)
	if err != nil {
		return err
	}
	return nil
}
