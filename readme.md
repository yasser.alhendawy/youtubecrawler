# youtubeCrawler

youtubeCrawler golang Project for crawling youtube playlist or channels.

## go mod

go mod is the dependency management tool is used internally .



## Usage
just validate url and start crawlling
```golang
url := "https://www.youtube.com/user/AsapSCIENCE/videos"
	if Validate(url) {
		youtubecrawler.Crawler(url, false)
	}
```
- Crawler func  take url that you will crawl and true variavble if you want to download all videos in the page  

## packages used internally
- [chromedp](https://godoc.org/github.com/chromedp/chromedp) for better and automated scraping
- [mongo](https://godoc.org/go.mongodb.org/mongo-driver/mongo) as database which has only one collection to store data
- "http://youtube.com/get_video_info?video_id=" is used to retrive videos metadata



## License
[MIT](https://choosealicense.com/licenses/mit/)