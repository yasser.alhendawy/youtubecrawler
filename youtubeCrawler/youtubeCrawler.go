package youtubecrawler

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/chromedp/chromedp"
	"gitlab.com/yasseralhendawy/youtubeCrawler/db"
	"gitlab.com/yasseralhendawy/youtubeCrawler/model"
	"golang.org/x/net/html"
)

//Crawler to crawel youtube link , downloadOption is true when we want to download Videos
func Crawler(url string, downloadOption bool) {
	n, err := chromedpReader(url)
	if err != nil {
		fmt.Println(err.Error())
	}
	links := GetLinks(n)
	videochan := make(chan model.Video)
	videochanOut := make(chan model.Video)
	errorchan := make(chan error)
	for i := range links {
		id, err := findVideoID(links[i])
		if err != nil {
			fmt.Println(err.Error())
		}
		videoInfo, err := GetVideoMetaDataINfo(id)
		if err != nil {
			fmt.Println(err.Error())
		}
		go videoInfoParser(videoInfo, links[i], videochan, errorchan)

	}
	go downloader(videochanOut, downloadOption)
	for r := range videochan {
		videochanOut <- r
	}
	for err := range errorchan {
		fmt.Println(err.Error())
	}
}

//chromedpReader to get better automated html result , this function return specific nood element in html file
func chromedpReader(url string) (*html.Node, error) {

	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()
	var body string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(url),
		chromedp.WaitNotVisible(`setTimeout`),
		//chromedp.WaitNotVisible(`#show-more-button`, chromedp.ByID),
		//chromedp.SendKeys(`#show-more-button`, `focus`, chromedp.ByID),
		//chromedp.Click(`#show-more-button`, chromedp.ByID),
		chromedp.Sleep(5*time.Second),
		chromedp.OuterHTML("html", &body),
	); err != nil {
		panic(err)
	}
	r := strings.NewReader(body)
	page, err := html.Parse(r)
	if err != nil {
		return nil, err
	}
	ok := false

	switch model.MyLink.LinkType {
	case "channel":
		page, ok = GetElement("items", "style-scope ytd-grid-renderer", page)
	case "playlist":
		page, ok = GetElement("items", "playlist-items yt-scrollbar-dark style-scope ytd-playlist-panel-renderer", page)
	}
	if !ok {
		err = errors.New("node not found ")
		return nil, err
	}

	return page, nil
}

// Reader to read target node  in specific link this function get source page (not used)
func Reader(url string, root bool) (*html.Node, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	page, err := html.Parse(res.Body)
	if err != nil {
		return nil, err
	}
	ok := false
	if root {
		switch model.MyLink.LinkType {
		case "channel":
			page, ok = GetElement("items", "style-scope ytd-grid-renderer", page)
		case "playlist":
			page, ok = GetElement("items", "playlist-items yt-scrollbar-dark style-scope ytd-playlist-panel-renderer", page)
		}
		if !ok {
			err = errors.New("node not found ")
			return nil, err
		}
	}
	return page, nil
}

// GetElement to get element in specific node by id and class (Reade & chromedpReader ) helper function
func GetElement(id string, class string, n *html.Node) (element *html.Node, ok bool) {
	idISTrue := false
	for _, a := range n.Attr {
		if a.Key == "id" && a.Val == id {
			idISTrue = true
			//	return n, true
		}
		if a.Key == "class" && a.Val == class && idISTrue {
			return n, true
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if element, ok = GetElement(id, class, c); ok {
			return
		}
	}
	return
}

//GetLinks from youtube page channel/playlist
func GetLinks(n *html.Node) []string {
	var links []string
	if n.Type == html.ElementNode && n.Data == "a" {
		taregetNode := false
		for _, a := range n.Attr {
			if a.Key == "id" && a.Val == "thumbnail" {
				taregetNode = true
				continue
			}
			if a.Key == "href" && taregetNode {
				links = append(links, "https://www.youtube.com/"+a.Val)
			}
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		exLinks := GetLinks(c)
		links = append(links, exLinks...)
	}
	return links
}

//findVideoID return video id
func findVideoID(url string) (string, error) {
	videoID := url
	if strings.Contains(videoID, "youtu") || strings.ContainsAny(videoID, "\"?&/<%=") {
		reList := []*regexp.Regexp{
			regexp.MustCompile(`(?:v|embed|watch\?v)(?:=|/)([^"&?/=%]{11})`),
			regexp.MustCompile(`(?:=|/)([^"&?/=%]{11})`),
			regexp.MustCompile(`([^"&?/=%]{11})`),
		}
		for _, re := range reList {
			if isMatch := re.MatchString(videoID); isMatch {
				subs := re.FindStringSubmatch(videoID)
				videoID = subs[1]
			}
		}
	}
	if strings.ContainsAny(videoID, "?&/<%=") {
		return "", errors.New("invalid characters in video id")
	}
	if len(videoID) < 10 {
		return "", errors.New("the video id must be at least 10 characters long")
	}
	return videoID, nil
}

// GetVideoMetaDataINfo to get videos information
func GetVideoMetaDataINfo(VideoID string) (string, error) {
	url := "http://youtube.com/get_video_info?video_id=" + VideoID
	//y.log(fmt.Sprintf("url: %s", url))
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return "", err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	//y.videoInfo = string(body)
	return string(body), nil
}

// videoInfoParser return video data which will be stored
func videoInfoParser(videoInfo string, videoURL string, videoChan chan model.Video, errorchan chan error) {
	var v model.Video
	response, err := url.ParseQuery(videoInfo)
	if err != nil {
		errorchan <- err
		return
	}
	status, ok := response["status"]
	if !ok {
		err = fmt.Errorf("no response status found in the server's answer")
		errorchan <- err
		return
	}
	if status[0] == "fail" {
		reason, ok := response["reason"]
		if ok {
			err = fmt.Errorf("'fail' response status found in the server's answer, reason: '%s'", reason[0])
		} else {
			err = errors.New(fmt.Sprint("'fail' response status found in the server's answer, no reason given"))
		}
		errorchan <- err
		return
	}
	if status[0] != "ok" {
		err = fmt.Errorf("non-success response status found in the server's answer (status: '%s')", status)
		errorchan <- err
		return

	}
	playerResponse := make(map[string]interface{})
	err = json.Unmarshal([]byte(response["player_response"][0]), &playerResponse)
	if err != nil {
		err = fmt.Errorf("player_response not found ")
		return
	}
	details := playerResponse["videoDetails"].(map[string]interface{})
	thumbnails := details["thumbnail"].(map[string]interface{})["thumbnails"].([]interface{})[0].(map[string]interface{})
	fullSizedthumbnails := playerResponse["microformat"].(map[string]interface{})["playerMicroformatRenderer"].(map[string]interface{})["thumbnail"].(map[string]interface{})["thumbnails"].([]interface{})[0].(map[string]interface{})

	//videoFormatsMap := playerResponse["streamingData"].(map[string]interface{})["formats"].([]interface{})
	v.URL = videoURL
	v.ID = details["videoId"].(string)
	v.Title = details["title"].(string)
	v.Duration = details["lengthSeconds"].(string)
	v.Views = details["viewCount"].(string)
	v.ThumbnailImageURL = thumbnails["url"].(string)
	v.FullSizedImageURL = fullSizedthumbnails["url"].(string)

	videoFormats := playerResponse["streamingData"]
	if videoFormats != nil {
		videoFormatsMap := playerResponse["streamingData"].(map[string]interface{})["formats"].([]interface{})
		for i := range videoFormatsMap {
			node := videoFormatsMap[i].(map[string]interface{})
			if node["quality"] == "medium" && node["url"] != nil {
				v.DownloadURL = node["url"].(string)
				break
			}
		}
	}
	videoChan <- v

}

// add video to MyLink and download images and videos localy
func downloader(videochan chan model.Video, downloadOption bool) {

	for v := range videochan {
		var err error
		/*if len(model.MyLink.Videos) <= 1 {
			db.InsertData()
		}*/
		if v.DownloadURL != "" && downloadOption {
			v.DownloadedVideoPath, err = downloadFiles(filepath.Join("Videos", v.Title), v.DownloadURL)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
		v.ThumbnailImagePath, err = downloadFiles(filepath.Join("ThumbnailImages", v.Title), v.ThumbnailImageURL)
		if err != nil {
			fmt.Println(err.Error())
		}
		v.FullSizedImagePath, err = downloadFiles(filepath.Join("FullSizedImages", v.Title), v.FullSizedImageURL)
		if err != nil {
			fmt.Println(err.Error())
		}
		model.MyLink.Videos = append(model.MyLink.Videos, v)
		db.UpsertData()
		fmt.Println(v.URL)
	}
}

func downloadFiles(destFile string, uri string) (string, error) {

	resp, err := http.Get(uri)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", errors.New("non 200 status code received")
	}
	err = os.MkdirAll(filepath.Dir(destFile), 0755)
	if err != nil {
		return "", err
	}
	out, err := os.Create(destFile)
	if err != nil {
		return "", err
	}
	mw := io.MultiWriter(out)
	_, err = io.Copy(mw, resp.Body)
	if err != nil {
		return "", err
	}

	return destFile, nil
}
