package model

// YoutubeCrawlingLink contains Videos details , link which will be crawel
type YoutubeCrawlingLink struct {
	URL      string  `bson:"url" json:"url"`
	LinkType string  `bson:"linkType" json:"linkType"`
	Videos   []Video `bson:"videos" json:"videos"`
}

// Video contains videos details
type Video struct {
	ID                  string `bson:"id" json:"id"`
	URL                 string `bson:"url" json:"url"`
	Title               string `bson:"title" json:"title"`
	Duration            string `bson:"duration" json:"duration"`
	Views               string `bson:"views" json:"views"`
	ThumbnailImageURL   string `bson:"thumbnailImageURL" json:"thumbnailImageURL"`
	ThumbnailImagePath  string `bson:"thumbnailImagePath" json:"thumbnailImagePath"`
	FullSizedImageURL   string `bson:"fullSizedImageURL" json:"fullSizedImageURL"`
	FullSizedImagePath  string `bson:"fullSizedImagePath" json:"fullSizedImagePath"`
	DownloadURL         string `bson:"downloadURL" json:"downloadURL"`
	DownloadedVideoPath string `bson:"downloadedVideoPath" json:"downloadedVideoPath"`
}

// MyLink the link we will crawel with all crawelling data
var MyLink YoutubeCrawlingLink
