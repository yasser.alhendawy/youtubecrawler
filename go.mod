module gitlab.com/yasseralhendawy/youtubeCrawler

go 1.13

require (
	github.com/chromedp/cdproto v0.0.0-20191027233732-553aa270fe0a // indirect
	github.com/chromedp/chromedp v0.5.1
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.1.2
	golang.org/x/net v0.0.0-20191021144547-ec77196f6094
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20191027211539-f8518d3b3627 // indirect
)
